//
//  GCAppDelegate.m
//  ArchiveTo7z
//
//  Created by Gene Crucean on 8/10/12.
//  Copyright (c) 2012 Gene Crucean. All rights reserved.
//
//  Read GCConstants.h for toggles and info.
//

#import "GCAppDelegate.h"
#import "GCConstants.h"

@implementation GCAppDelegate
@synthesize outputFileLabel;
@synthesize outputStatusLabel;
@synthesize mainWindow;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    LOG3 ? NSLog(@"applicationDidFinishLaunching") : nil;
    
    [outputStatusLabel setStringValue:@"Application is designed to have files dropped onto the icon in the dock."];
}


- (void)application:(NSApplication *)theApplication openFiles:(NSArray *)filenames
{
    LOG3 ? NSLog(@"openFiles") : nil;
    
    [outputStatusLabel setStringValue:@"Archiving files..."];
    
    if (filenames.count == 0)
    {
//        exit(0);
        [outputStatusLabel setStringValue:@"Application is designed to have files dropped onto the icon in the dock."];
    }
    else
    {
        [outputStatusLabel setStringValue:@"Archiving files..."];
        [self processFiles:filenames];
    }
    
    if (isWindowless)
    {
        [_window setIsVisible:NO];
    }
    else
    {
        [_window setIsVisible:YES];
    }
}


- (BOOL)processFiles:(NSArray *)files
{
    NSLog(@"The following file has been dropped or selected: %@", files);
    
    // Set GUI's label so users can see what files were input.
    [outputFileLabel setStringValue:[NSString stringWithFormat:@"%@", files]];
    
    // Create NSTask.
    task = [[NSTask alloc] init];
    
    // Build path to 7za binary.
    NSString *path = [[NSBundle mainBundle] resourcePath];
    NSString *pathFull = [path stringByAppendingPathComponent:@"7za"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    // Make sure 7za binary is in the proper location.
    if ([fileManager fileExistsAtPath:pathFull]) // 7za exists.
    {
        LOG3 ? NSLog(@"fileExists") : nil;
        LOG5 ? NSLog(@"launchPath: %@", pathFull) : nil;
        
        //        [task setLaunchPath:@"/bin/bash"];
        [task setLaunchPath:pathFull];
    }
    
    // Tell NSTask that you want to Archive. a is archive flag doi.
    NSMutableArray *argumentBuilder = [[NSMutableArray alloc] init];
    [argumentBuilder addObject:@"a"]; // Set archive flag.
    
    // Get parent folder name.
    NSString *parentFolderName = [NSString stringWithFormat:@"%@", [[[files objectAtIndex:0] stringByDeletingLastPathComponent] lastPathComponent]];
    LOG5 ? NSLog(@"Parent folder name: %@", parentFolderName) : nil;
    
    // If more than 1 file... handle accordingly.
    NSString *fileOutputPath;
    if (files.count == 1)
    {
        fileOutputPath = [NSString stringWithFormat:@"%@.7z", [files objectAtIndex:0]]; // Format output file.
        LOG5 ? NSLog(@"fileOutputPath: %@", fileOutputPath) : nil;
    }
    else
    {
        fileOutputPath = [NSString stringWithFormat:@"%@/%@.7z", [[files objectAtIndex:0] stringByDeletingLastPathComponent], parentFolderName]; // Format output file.
        LOG5 ? NSLog(@"fileOutputPath: %@", fileOutputPath) : nil;
    }
    
    [argumentBuilder addObject:fileOutputPath]; // Set as argument.
    
    // Loop through input files and add to argument builder.
    for (int i = 0; i < files.count; i++)
    {
        [argumentBuilder addObject:[files objectAtIndex:i]]; // Add path to files to archive.
        LOG5 ? NSLog(@"inputFile: %@", [files objectAtIndex:i]) : nil;
    }
    
    // debug.
//    NSLog(@"argumentBuilder:%@", argumentBuilder); // Log the input arguments.
    
    // Set arguments.
    [task setArguments:argumentBuilder];
    
    // Launch task
    [task launch];
    
    // Set timer to update GUI and exit after task finishes.
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(testIfTaskIsRunning) userInfo:nil repeats:YES];
    
    return YES; // Return YES when file processed succesfull, else return NO.
}



- (void)testIfTaskIsRunning
{
    if (![task isRunning])
    {
        int status = [task terminationStatus];
        if (status == 0)
        {
            NSLog(@"Task succeeded.");
            [outputStatusLabel setStringValue:@"Archiving complete. Exiting in 3 seconds."];
            [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(exitApp) userInfo:nil repeats:NO];
            
            if (isWindowless)
            {
                if (!didPlaySound)
                {
                    NSSound *sound;
                    sound = [[NSSound alloc] initWithContentsOfFile:@"/System/Library/Sounds/Glass.aiff" byReference:YES];
                    [sound play];
                    didPlaySound = YES;
                }
//                [_window setIsVisible:YES];
//                [_window setOrderedIndex:0];
            }
        }
        else
        {
            NSLog(@"Task failed.");
            [outputStatusLabel setStringValue:@"Oh noes! Something bombed and the task failed. Exiting in 3 seconds."];
            [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(exitApp) userInfo:nil repeats:NO];
            
            if (isWindowless)
            {
                if (!didPlaySound)
                {
                    NSSound *sound;
                    sound = [[NSSound alloc] initWithContentsOfFile:@"/System/Library/Sounds/Basso.aiff" byReference:YES];
                    [sound play];
                    didPlaySound = YES;
                }
                
                [_window setIsVisible:YES];
                [_window setOrderedIndex:0];
            }
        }
    }
    else
    {
        LOG4 ? NSLog(@"Task is still running.") : nil;
        [outputStatusLabel setStringValue:@"Archiving in progress..."];
    }
}


- (void)exitApp
{
    exit(0); // BOOSH!! See ya!
}


@end
