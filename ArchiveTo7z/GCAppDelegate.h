//
//  GCAppDelegate.h
//  ArchiveTo7z
//
//  Created by Gene Crucean on 8/10/12.
//  Copyright (c) 2012 Gene Crucean. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface GCAppDelegate : NSObject <NSApplicationDelegate>
{
    NSTask *task;
    NSWindowController *mainWindow;
    BOOL didPlaySound;
}


@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, retain) NSWindowController *mainWindow;
@property (weak) IBOutlet NSTextField *outputFileLabel;
@property (weak) IBOutlet NSTextField *outputStatusLabel;


- (BOOL)processFiles:(NSArray *)files;


@end
